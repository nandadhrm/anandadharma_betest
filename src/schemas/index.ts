import mongoose from "mongoose";
import { UserInfo } from "./UserInfo"
import { AccountLogin } from "./AccountLogin";
import type { UserInfoAttributes } from "./UserInfo";
import type { AccountLoginAttributes } from "./AccountLogin";

try {

    /** Connect to db */
    // mongoose
    //     .connect(`mongodb://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}?authSource=${process.env.DB_AUTH_NAME}`)
    // console.info(`[server] Connected to database ${process.env.DB_NAME}: ${process.env.DB_HOST}:${process.env.DB_PORT}`)

} catch (error) {
    console.error(`[server] Unable to connect to the database ${process.env.DB_NAME}: ${process.env.DB_HOST}:${process.env.DB_PORT}`)
}

export {
    UserInfo,
    AccountLogin
}

export {
    UserInfoAttributes,
    AccountLoginAttributes
}