import {
    Schema, model
} from "mongoose"

// Create type interface for accountLogin collection
export interface AccountLoginAttributes {
    accountId: string,
    userName: string,
    password: string,
    lastLoginDateTime: Date,
    userId: string
}

// Define mongoose schema for accountLogin collection
const accountLoginSchema = new Schema<AccountLoginAttributes>({
    accountId: { type: String},
    userName: { type: String},
    password: { type: String},
    lastLoginDateTime: { type: Date},
    userId: { type: String}
})

// Define userinfo mongoose model and export
export const AccountLogin = model<AccountLoginAttributes>('accountLogin', accountLoginSchema, 'accountLogin');