import {
    Schema, model
} from "mongoose"

// Create type interface for userinfo collection
export interface UserInfoAttributes {
    userId: string,
    fullName: string,
    accountNumber: string,
    emailAddress: string,
    registrationNumber: string
}

// Define mongoose schema for userinfo collection
const userInfoSchema = new Schema<UserInfoAttributes>({
    userId: { type: String},
    fullName: { type: String},
    accountNumber: { type: String},
    emailAddress: { type: String},
    registrationNumber: { type: String}
})

// Define userinfo mongoose model and export
export const UserInfo = model<UserInfoAttributes>('userinfo', userInfoSchema, 'userinfo');