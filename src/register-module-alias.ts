// Paths Alias
import "module-alias/register";
import { addAliases } from "module-alias";

addAliases({
    "@/": `${__dirname}/`,
    "@/configs": `${__dirname}/configs`,
    "@/middlewares": `${__dirname}/middlewares`,
    // "@/models": `${__dirname}/models`,
    "@/schemas": `${__dirname}/schemas`,
    "@/modules": `${__dirname}/modules`,
    "@/routes": `${__dirname}/routes`,
    "@/shared": `${__dirname}/shared`,
});
