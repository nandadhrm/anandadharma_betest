export const NODE_ENV = <string>(process.env.NODE_ENV || "development");

export const APP_PORT = <number>+(process.env.APP_PORT || 3000);
export const APP_NAME = <string>(process.env.APP_NAME || "ms-anandadharma-betest");

export const JWT_SECRET = <string>process.env.JWT_SECRET;
export const JWT_EXPIRED_SECONDS = <number>+(process.env.JWT_EXPIRED_SECONDS || 7200);
export const JWT_REFRESH_EXPIRED_SECONDS = <number>+(process.env.JWT_REFRESH_EXPIRED_SECONDS || 43200);
