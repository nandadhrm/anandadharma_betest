import { Kafka, KafkaConfig } from "kafkajs";

const kafkaConfig: KafkaConfig = {
    clientId: "ms_anandadharma_betest",
    brokers: [`${process.env.KAFKA_BROKER_HOST}`],
    connectionTimeout: 1000,
    requestTimeout: 20000
}

export const KAFKA_MAIN_TOPIC = <string>process.env.KAFKA_MAIN_TOPIC;
export const mainBroker = new Kafka(kafkaConfig);