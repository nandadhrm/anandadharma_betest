import { JWT_EXPIRED_SECONDS, JWT_REFRESH_EXPIRED_SECONDS, JWT_SECRET } from "@/configs";
import { userService } from "@/modules/user";
import { HttpStatus, UnauthorizedErrorException } from "@/shared";
import { ICreateJWTResult, JWT_TOKEN_TYPE } from "./authentication.type";
import jwt from "jsonwebtoken";

export async function login(username: string, password: string): Promise<ICreateJWTResult> {

    let user = await userService.getAccountLoginByUserName(username);
    if (!user) {
        throw new UnauthorizedErrorException("Invalid credentials");
    }

    const resultToken = await createJWT({ userUid: user?.userId });

    return resultToken;
}

export async function createJWT({ userUid }: { userUid: string }): Promise<ICreateJWTResult> {
    const accessToken = jwt.sign({ userUid, type: JWT_TOKEN_TYPE.ACCESS_TOKEN }, JWT_SECRET, { expiresIn: JWT_EXPIRED_SECONDS });
    const refreshToken = jwt.sign({ userUid, type: JWT_TOKEN_TYPE.REFRESH_TOKEN }, JWT_SECRET, { expiresIn: JWT_REFRESH_EXPIRED_SECONDS });

    return {
        accessToken: accessToken,
        expiresIn: JWT_EXPIRED_SECONDS,
        refreshToken: refreshToken,
    };
}