import { HttpStatus, ResponseHandler, UnauthorizedErrorException } from "@/shared";
import { Request, Response } from "express";
import * as authenticationService from "./authentication.service";
import { ILoginRequest } from "./authentication.type";

export async function login(req: Request, res: Response): Promise<void> {

    const authorization = req.headers.authorization?.split(" ");

    if (!authorization || authorization[0] !== "Basic" || !authorization[1]) {
        throw new UnauthorizedErrorException("Invalid login");
    }

    const encoded = Buffer.from(authorization[1], "base64").toString("ascii");
    const [username, password] = encoded.split(":");

    if (!username || !password) {
        throw new UnauthorizedErrorException("Invalid login");
    }
    
    const payload: ILoginRequest = {
        username,
        password
    }

    const resultToken = await authenticationService.login(payload.username, payload.password);

    return ResponseHandler(res).send({ httpStatus: HttpStatus.OK, message: "Login success", data: resultToken });
}