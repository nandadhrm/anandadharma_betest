export interface ILoginRequest {
    username: string;
    password: string;
}

export interface ICreateJWTResult {
    accessToken: string;
    expiresIn: number;
    refreshToken: string;
}

export enum JWT_TOKEN_TYPE {
    ACCESS_TOKEN = "access_token",
    REFRESH_TOKEN = "refresh_token",
}