import { UserInfoAttributes, UserInfo, AccountLogin, AccountLoginAttributes } from "@/schemas";
import { nanoid } from "nanoid";
import { mainBroker, KAFKA_MAIN_TOPIC } from "@/configs";

/** UserInfo */
export async function getUserInfoByAccountNumber(accountNumber: string): Promise<UserInfoAttributes> {
    // Find userinfo by accountNumber
    const user: any = await UserInfo.findOne({ accountNumber: accountNumber }).exec();

    // declare producer
    const producer = mainBroker.producer({
        // allowAutoTopicCreation: false,
        transactionTimeout: 10000
    });

    // connect producer to broker
    await producer.connect();

    // produce message
    await producer.send({
        topic: KAFKA_MAIN_TOPIC,
        messages: [
            { key: `betest_${nanoid(8)}`, value: JSON.stringify(user) }
        ]
    })

    // disconnect producer from broker
    await producer.disconnect();

    return user;
}

export async function getUserInfoByRegistrationNumber(registrationNumber: string): Promise<UserInfoAttributes> {
    // Find userinfo by registrationNumber
    const user: any = await UserInfo.findOne({ registrationNumber: registrationNumber }).exec();
    return user;
}

export async function createUserInfo(payload: any): Promise<void> {
    try {
        // Save new userinfo object to db
        await UserInfo.create({
            userId: nanoid(25),
            fullName: payload.fullName,
            accountNumber: nanoid(20),
            emailAddress: payload.emailAddress,
            registrationNumber: nanoid(20)
        })
    } catch (error) {
        console.error(error)
        throw error;
    }
}

export async function updateUserInfo(payload: any): Promise<void> {
    try {
        // find userinfo by userId and update data
        await UserInfo.findOneAndUpdate({ userId: payload.userId }, {
            fullName: payload.fullName,
            emailAddress: payload.emailAddress,
        })
    } catch (error) {
        console.error(error)
        throw error;
    }
}

export async function removeUserInfo(userId: string): Promise<void> {
    try {
        // find userinfo by userId and remove data
        await UserInfo.findOneAndDelete({ userId: userId });
    } catch (error) {
        console.error(error)
        throw error;
    }
}

/** Account Login */
export async function getAccountLoginByUserName(username: string): Promise<AccountLoginAttributes> {
    // Find userinfo by registrationNumber
    const user: any = await AccountLogin.findOne({ userName: username }).exec();
    return user;
}

export async function getAccountLoginByLoginDay(): Promise<any> {
    // Find userinfo by accountNumber
    const user: any = await AccountLogin.find({}).exec();

    const arr: string[] = [];
    const today: number = new Date().getTime();

    docLoop:for (const val of user) {
        
        const lastLoginDateTime: number = new Date(val.lastLoginDateTime).getTime();
        const diff: number = Math.round((today - lastLoginDateTime) / (1000 * 3600 * 24));

        if (diff > 3) arr.push(val);
        else continue docLoop;
        
    }

    return arr;
}

export async function createAccountLogin(payload: any): Promise<void> {
    try {
        // Save new userinfo object to db
        await AccountLogin.create({
            accountId: payload.userId,
            userName: payload.userName,
            password: payload.password,
            lastLoginDateTime: new Date(),
            userId: payload.userId
        })
    } catch (error) {
        console.error(error)
        throw error;
    }
}

export async function updateAccountLogin(payload: any): Promise<void> {
    try {
        // find userinfo by userId and update data
        await AccountLogin.findOneAndUpdate({ userId: payload.userId }, {
            userName: payload.userName,
            password: payload.password
        })
    } catch (error) {
        console.error(error)
        throw error;
    }
}

export async function removeAccountLogin(userId: string): Promise<void> {
    try {
        // find userinfo by userId and remove data
        await AccountLogin.findOneAndDelete({ userId: userId });
    } catch (error) {
        console.error(error)
        throw error;
    }
}