export interface IGetUserInfoRequest {
    accountNumber: string;
    registrationNumber: string;
}

export interface ICreateUserInfoRequest {
    emailAddress: string;
    fullName: string;
}


export interface IUpdateUserInfoRequest {
    userId: string;
    emailAddress: string;
    fullName: string;
}

export interface ICreateAccountLoginRequest {
    userId: string;
    userName: string;
    password: string;
}

export interface IUpdateAccountLoginRequest {
    userId: string;
    userName: string;
    password: string;
}