import { Request, Response } from "express";
import { ResponseHandler, HttpStatus, BadRequestErrorException, InternalServerErrorException } from "@/shared";
import * as UserService from "./user.service"
import { IGetUserInfoRequest, ICreateUserInfoRequest, IUpdateUserInfoRequest, ICreateAccountLoginRequest, IUpdateAccountLoginRequest } from "./user.type";

export const getUser = async (req: Request, res: Response): Promise<void> => {

    const params: any = req.query

    const accountNumber = params.accountNumber;
    const registrationNumber = params.registrationNumber;

    if (!accountNumber && !registrationNumber) {
        throw new BadRequestErrorException("Invalid params");
    }

    const query: IGetUserInfoRequest = {
        accountNumber,
        registrationNumber
    };

    if (query.accountNumber) {

        const user = await UserService.getUserInfoByAccountNumber(query.accountNumber)

        return ResponseHandler(res).send({ httpStatus: HttpStatus.OK, message: "userinfo fetched" ,data: user });

    } else if (query.registrationNumber) {

        const user = await UserService.getUserInfoByRegistrationNumber(query.registrationNumber)

        return ResponseHandler(res).send({ httpStatus: HttpStatus.OK, message: "userinfo fetched", data: user });

    }

}

export const registerUser = async (req: Request, res: Response): Promise<void> => {

    if (!req.body?.fullName || !req.body?.emailAddress) {
        throw new BadRequestErrorException("Invalid Payload");
    }

    const payload: ICreateUserInfoRequest = {
        emailAddress: req.body.emailAddress,
        fullName: req.body.fullName
    };

    try {
        await UserService.createUserInfo(payload);
    } catch (error: any) {
        throw new InternalServerErrorException("Failed to create userinfo", error);
    }

    return ResponseHandler(res).send({ httpStatus: HttpStatus.OK, message: "Create userinfo success",data: {} });
}

export const editUser = async (req: Request, res: Response): Promise<void> => {

    if (!req.body?.fullName || !req.body?.emailAddress) {
        throw new BadRequestErrorException("Invalid Payload");
    }

    const payload: IUpdateUserInfoRequest = {
        userId: req.body.userId,
        emailAddress: req.body.emailAddress,
        fullName: req.body.fullName
    };

    try {
        await UserService.updateUserInfo(payload);
    } catch (error: any) {
        throw new InternalServerErrorException("Failed to update userinfo", error);
    }

    return ResponseHandler(res).send({ httpStatus: HttpStatus.OK, message: "Update userinfo success",data: { userId: payload.userId } });
}

export const removeUser = async (req: Request, res: Response): Promise<void> => {

    if (!req.body?.userId) {
        throw new BadRequestErrorException("Invalid Payload");
    }

    const userId: string = req.body.userId;

    try {
        await UserService.removeUserInfo(userId);
    } catch (error: any) {
        throw new InternalServerErrorException("Failed to remove userinfo", error);
    }

    return ResponseHandler(res).send({ httpStatus: HttpStatus.OK, message: "Remove userinfo success", data: { userId: userId } });
}

export const getAccountLogin = async (req: Request, res: Response): Promise<void> => {

    const loginHistory = await UserService.getAccountLoginByLoginDay();

    return ResponseHandler(res).send({ httpStatus: HttpStatus.OK, message: "Login history fetched", data: loginHistory });

}

export const insertAccountLogin = async (req: Request, res: Response): Promise<void> => {

    if (!req.body?.userId || !req.body?.userName || !req.body?.password) {
        throw new BadRequestErrorException("Invalid Payload");
    }

    const payload: ICreateAccountLoginRequest = req.body;

    try {
        await UserService.createAccountLogin(payload);
    } catch (error: any) {
        throw new InternalServerErrorException("Failed to create accountLogin", error);
    }

    return ResponseHandler(res).send({ httpStatus: HttpStatus.OK, message: "Create accountLogin success",data: {} });
}

export const editAccountLogin = async (req: Request, res: Response): Promise<void> => {

    if (!req.body?.userId || !req.body?.userName || !req.body?.password) {
        throw new BadRequestErrorException("Invalid Payload");
    }

    const payload: IUpdateAccountLoginRequest = req.body;

    try {
        await UserService.updateAccountLogin(payload);
    } catch (error: any) {
        throw new InternalServerErrorException("Failed to update userinfo", error);
    }

    return ResponseHandler(res).send({ httpStatus: HttpStatus.OK, message: "Update accountLogin success",data: { userId: payload.userId } });
}

export const removeAccountLogin = async (req: Request, res: Response): Promise<void> => {

    if (!req.body?.userId) {
        throw new BadRequestErrorException("Invalid Payload");
    }

    const userId: string = req.body.userId;

    try {
        await UserService.removeAccountLogin(userId);
    } catch (error: any) {
        throw new InternalServerErrorException("Failed to remove accountLogin", error);
    }

    return ResponseHandler(res).send({ httpStatus: HttpStatus.OK, message: "Remove accountLogin success", data: { userId: userId } });
}