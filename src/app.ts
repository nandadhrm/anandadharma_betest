import "./register-module-alias";
import dotenv from "dotenv";
dotenv.config();

import express, { Request, Response, NextFunction, ErrorRequestHandler } from "express";
import routes from "@/routes/index";
import morganBody from "morgan-body";
import { ResponseHandler, HttpStatus, ErrorHandler, ErrorFilter } from "@/shared";

const app = express();

import { APP_NAME, APP_PORT } from "@/configs";

class MainApp {
    public static start(): void {
        morganBody(app);

        app.disable("x-powered-by");

        /** Interceptors */
        app.use(express.urlencoded({ extended: true, limit: "5mb" }));
        app.use(express.json({ limit: "5mb" }));

        /** Routes */
        app.use(routes);

        /** Filter */
        app.use(ErrorFilter);

        /** Not Found Route */
        app.all("*", (req, res, next) => !res.headersSent && ResponseHandler(res).send({ httpStatus: HttpStatus.NOT_FOUND }));

        /** Start Server */
        app.listen(APP_PORT, () => {
            console.info(`[server] ${APP_NAME} is running on port ${APP_PORT}`);
        });
    }
}

MainApp.start();
