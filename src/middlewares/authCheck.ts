import { Request, Response, NextFunction, response } from "express";
import { HttpStatus, ResponseHandler } from "@/shared";
import { JWT_SECRET } from "@/configs";
import jwt from "jsonwebtoken";

export async function AuthCheck (req: Request, res: Response, next: NextFunction) {
    const authorization: any = req.headers.authorization?.split(" ");

    if (!authorization || authorization[0] !== "Bearer" || !authorization[1]) {
        return ResponseHandler(res).send({ httpStatus: HttpStatus.UNAUTHORIZED, message: "Unauthorized Access" });
    }

    const token: string = authorization[1];

    try {
        jwt.verify(token, JWT_SECRET);
        next();
    } catch(err) {
        return ResponseHandler(res).send({ httpStatus: HttpStatus.UNAUTHORIZED, message: "Unauthorized Access" });
    }

};
