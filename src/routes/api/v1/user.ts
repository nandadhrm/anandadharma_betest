import { Router } from "express";
import { ErrorHandler } from "@/shared";
import { userController } from "@/modules/user";

const router = Router();

router
    .get("/account-login", ErrorHandler(userController.getAccountLogin))
    .post("/account-login", ErrorHandler(userController.insertAccountLogin))
    .put("/account-login", ErrorHandler(userController.editAccountLogin))
    .delete("/account-login", ErrorHandler(userController.removeAccountLogin))

router
    .get("/user-info", ErrorHandler(userController.getUser))
    .post("/user-info", ErrorHandler(userController.registerUser))
    .put("/user-info", ErrorHandler(userController.editUser))
    .delete("/user-info", ErrorHandler(userController.removeUser));

export default router;