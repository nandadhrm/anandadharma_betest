import { Router } from "express";
import UserRoute from "./user";
import AuthRoute from "./authentication";
import { AuthCheck } from "@/middlewares";

const router = Router();

router.use("/auth", AuthRoute);
router.use("/user", AuthCheck, UserRoute);

export default router;