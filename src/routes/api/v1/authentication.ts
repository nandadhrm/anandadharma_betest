import { Router } from "express";
import { ErrorHandler } from "@/shared";
import { authenticationController } from "@/modules/authentication";

const router = Router();

router.post("/", ErrorHandler(authenticationController.login));

export default router;