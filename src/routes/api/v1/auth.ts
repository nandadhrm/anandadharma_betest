import { Router } from "express";
import { ErrorHandler } from "@/shared";
import { contentController } from "@/modules/content";

const router = Router();

router.get("/", ErrorHandler(contentController.getExperiences));

export default router;