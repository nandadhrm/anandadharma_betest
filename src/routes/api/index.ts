import { Router } from "express";
import routeV1 from "./v1";

const router = Router();

router.use("/v1", routeV1);

export default router;