import { Request, Response, NextFunction } from "express";
import { BadRequestErrorException, ForbiddenErrorException, InternalServerErrorException, NotFoundException, ResponseHandler, UnauthorizedErrorException } from "../handler";
import { HttpStatus } from "../enum";

export function ErrorFilter (error: Error, req: Request, res: Response, next: NextFunction) {
    switch (error.constructor) {
        case SyntaxError: {
            return ResponseHandler(res).send({ httpStatus: HttpStatus.BAD_REQUEST, message: error?.message });
        }
        case InternalServerErrorException: {
            return ResponseHandler(res).send({ httpStatus: HttpStatus.INTERNAL_SERVER_ERROR, message: error?.message });
        }
        case BadRequestErrorException: {
            return ResponseHandler(res).send({ httpStatus: HttpStatus.BAD_REQUEST, message: error?.message });
        }
        case UnauthorizedErrorException: {
            return ResponseHandler(res).send({ httpStatus: HttpStatus.UNAUTHORIZED, message: error?.message });
        }
        case ForbiddenErrorException: {
            return ResponseHandler(res).send({ httpStatus: HttpStatus.FORBIDDEN, message: error?.message });
        }
        case NotFoundException: {
            return ResponseHandler(res).send({ httpStatus: HttpStatus.NOT_FOUND, message: error?.message });
        }
        default: {
            return ResponseHandler(res).send({ httpStatus: HttpStatus.INTERNAL_SERVER_ERROR, message: error?.message });
        }
    }
}