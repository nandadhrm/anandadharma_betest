import axios from "axios";

const axiosInstance = axios.create({
    timeout: 60000,
});

export default axiosInstance;