import { Response } from "express";

import { HttpStatus, getHttpReason } from "@/shared/enum";

export const ResponseHandler = (res: Response) => ({
    send: ({ httpStatus = HttpStatus.OK, message, data = {} }: { httpStatus: HttpStatus, message?: string, data?: Record<string, any> | Array<any> }) => {
        const response = {
            responseCode: `${httpStatus}`,
            responseMessage: message || getHttpReason(httpStatus),
            responseData: data
        }

        res.status(httpStatus);
        res.json(response);
        res.end();
    },
});
