import { Request, Response, NextFunction } from "express";

export const ErrorHandler = (fn: (req: Request, res: Response, next: NextFunction) => Promise<void>) => {
    return async function (req: Request, res: Response, next: NextFunction) {
        let e: Error | null = null;
        try {
            await fn(req, res, next);
        } catch (err) {
            e = <Error | null>err;
            next(err);
        }

        if (!e) {
            next();
        }
    };
};
