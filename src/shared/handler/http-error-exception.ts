
export class InternalServerErrorException extends Error {
    constructor(message: string, data?: Record<string, any>) {
        super(message);
        this.name = "InternalServerErrorException";
    }
}

export class BadRequestErrorException extends Error {
    constructor(message: string, data?: Record<string, any>) {
        super(message);
        this.name = "BadRequestErrorException";
    }
}

export class UnauthorizedErrorException extends Error {
    constructor(message: string, data?: Record<string, any>) {
        super(message);
        this.name = "UnauthorizedErrorException";
    }
}

export class ForbiddenErrorException extends Error {
    constructor(message: string, data?: Record<string, any>) {
        super(message);
        this.name = "ForbiddenErrorException";
    }
}

export class NotFoundException extends Error {
    constructor(message: string, data?: Record<string, any>) {
        super(message);
        this.name = "NotFoundException";
    }
}