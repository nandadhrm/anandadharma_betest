FROM node:16-alpine
WORKDIR /usr/src/app
ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install app dependencies
COPY package.json /app
COPY yarn.lock /app
RUN yarn

# Bundle app source
COPY . .
RUN yarn build

EXPOSE 3000
CMD [ "yarn", "start" ]
# RUN yarn install pm2 -g
# CMD ["pm2-runtime", "pm2-process.yaml"]